const express = require('express');
const Sequelize = require('sequelize');
const app = express();

const sequelize = new Sequelize('parking', 'root', '', {
    dialect: "mysql",
    host: "localhost"
});

sequelize.authenticate().then(() => {
    console.log("Connected to database");
}).catch(() => {
    console.log("Unable to connect to database");
});

const Addresses = sequelize.define('addresses', {
    id_address: { type: Sequelize.INTEGER, primaryKey:true, autoIncrement:true},
    country: Sequelize.TEXT,
    city: Sequelize.TEXT,
    district: Sequelize.TEXT,
    street: Sequelize.TEXT,
    number:Sequelize.INTEGER
});

const Parking_garages = sequelize.define('parking_garages', {
    id_garage: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    id_adress: Sequelize.INTEGER,
    storey_no: Sequelize.INTEGER,
    size: Sequelize.INTEGER,
    available_spots_no: Sequelize.INTEGER,
    park_fee: Sequelize.INTEGER
});

Parking_garages.belongsTo(Addresses);

const Parking_lots = sequelize.define('parking_lots', {
    id_lot: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    id_garage: {type: Sequelize.INTEGER, references: {
        model: Parking_garages,
        key: 'id_garage',
        // deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
    }
    },
    availability: Sequelize.BOOLEAN,
    lot_no: Sequelize.INTEGER,
    floor: Sequelize.INTEGER
});

const Drivers= sequelize.define('drivers', {
    id_driver: { type: Sequelize.INTEGER, primaryKey:true, autoIncrement:true},
    name: Sequelize.TEXT,
    phone:Sequelize.TEXT,
    email:Sequelize.TEXT
});

const Vehicles= sequelize.define('vehicles', {
    id_vehicle: {type: Sequelize.INTEGER, primaryKey:true, autoIncrement:true},
    id_driver: {type: Sequelize.INTEGER, references: {
        model:Drivers,
        key: 'id_driver',
        // deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
    }
    },
    license_plate: Sequelize.TEXT
});

const Reservations = sequelize.define('reservations',{
  id_reservation: {type:Sequelize.INTEGER, primaryKey:true, autoIncrement:true},
  id_driver:{type: Sequelize.INTEGER, references: {
        model:Drivers,
        key: 'id_driver',
        // deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
    }
    },
    id_vehicle:{type:Sequelize.INTEGER, references:{
        model:Vehicles,
        key:'id_vehicle',
        // deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
    }
    },
    id_lot:{type:Sequelize.INTEGER, references:{
        model:Parking_lots,
        key:'id_lot',
        // deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
    }
    },
    reservation_date: Sequelize.DATE,
    start_time:Sequelize.DATE,
    end_time:Sequelize.DATE
});

sequelize.sync({force: true}).then(()=>{
    console.log('Tables created successfully');
});

app.use(express.json());
app.use(express.urlencoded());

//ADDRESSES

//creez o noua inregistrare

// app.post('/add-address', (req, res) =>{
//     Addresses.create({
//         id_address: req.body.id_address,
//         country: req.body.country,
//         city: req.body.city,
//         district: req.body.district,
//         street: req.body.street,
//         number:req.body.number
//     }).then((user) => {
//         res.status(200).send("Garage address created successfully");
//     }, (err) => {
//         res.status(500).send(err);
//     });
// });

app.post('/addresses', (request, response) => {
    Addresses.create(request.body).then((result) => {
        response.status(201).json(result);
    }).catch((err) => {
        response.status(500).send("resource not created");
    });
});

//gasesc o inregistrare

// app.post('/find-address', (req, res) => {
//   Addresses.findOne({where:{id_address: req.body.id_address} }).then((result) => {
//       res.status(200).send(result)
//   }) 
// });


app.get('/addresses', (request, response) => {
    Addresses.findAll().then((results) => {
        response.status(200).json(results);
    });
});

app.get('/addresses/:id', (request, response) => {
    Addresses.findById(request.params.id).then((result) => {
        if(result) {
            response.status(200).json(result);
        } else {
            response.status(404).send('resource not found');
        }
    }).catch((err) => {
        console.log(err);
        response.status(500).send('database error');
    });
});

// //actualizez inregistrare
app.put('/addresses/:id', (request, response) => {
    Addresses.findById(request.params.id).then((street) => {
        if(street) {
            street.update(request.body).then((result) => {
                response.status(201).json(result);
            }).catch((err) => {
                console.log(err);
                response.status(500).send('database error');
            });
        } else {
            response.status(404).send('resource not found');
        }
    }).catch((err) => {
        console.log(err);
        response.status(500).send('database error');
    });
});

// //sterg inregistrare
app.delete('/addresses/:id', (request, response) => {
    Addresses.findById(request.params.id).then((street) => {
        if(street) {
            street.destroy().then((result) => {
                response.status(204).send();
            }).catch((err) => {
                console.log(err);
                response.status(500).send('database error');
            });
        } else {
            response.status(404).send('resource not found');
        }
    }).catch((err) => {
        console.log(err);
        response.status(500).send('database error');
    });
});

//PARKING_GARAGES

//creez o noua inregistrare

app.post('/garages', (request, response) => {
    Parking_garages.create(request.body).then((result) => {
        response.status(201).json(result);
    }).catch((err) => {
        response.status(500).send("resource not created");
    });
});

// //gasesc o inregistrare

app.get('/garages', (request, response) => {
    Parking_garages.findAll().then((results) => {
        response.status(200).json(results);
    });
});

app.get('/garages/:id', (request, response) => {
    Parking_garages.findById(request.params.id).then((result) => {
        if(result) {
            response.status(200).json(result);
        } else {
            response.status(404).send('resource not found');
        }
    }).catch((err) => {
        console.log(err);
        response.status(500).send('database error');
    });
});

//actualizez inregistrare
app.put('/garages/:id', (request, response) => {
    Parking_garages.findById(request.params.id_garage).then((available_spots_no) => {
        if(available_spots_no) {
            available_spots_no.update(request.body).then((result) => {
                response.status(201).json(result);
            }).catch((err) => {
                console.log(err);
                response.status(500).send('database error');
            });
        } else {
            response.status(404).send('resource not found');
        }
    }).catch((err) => {
        console.log(err);
        response.status(500).send('database error');
    });
});

//sterg inregistrare
app.delete('/garages/:id', (request, response) => {
    Parking_garages.findById(request.params.id_garage).then((available_spots_no) => {
        if(available_spots_no) {
            available_spots_no.destroy().then((result) => {
                response.status(204).send();
            }).catch((err) => {
                console.log(err);
                response.status(500).send('database error');
            });
        } else {
            response.status(404).send('resource not found');
        }
    }).catch((err) => {
        console.log(err);
        response.status(500).send('database error');
    });
});

app.use('/', express.static('public'));

app.listen(8080);